# Konzertmeister-statistics

Erzeugt aus der [Anwesenheitsstatistik](https://web.konzertmeister.app/attendance/statistics) der [Konzertmeister App](https://konzertmeister.app/de) Grafiken für die Auswertung der Anwesenheit bei Proben und Auftritten.