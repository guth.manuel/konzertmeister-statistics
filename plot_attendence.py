import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mtp
import numpy as np
from collections import Counter
from datetime import datetime
import argparse
import os


# TODO: add checks for duplicated musicians, duplicated concert names

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input_data", required=True)
parser.add_argument("-o", "--output_name", default="Test")
parser.add_argument("--folder", default="plots")
parser.add_argument("-d", "--delimiter", default=";")
parser.add_argument("-f", "--format", default="pdf")
parser.add_argument("-v", "--organisation", default="")
parser.add_argument(
    "-a", "--anonymous", action="store_true", help="anonymisiert Statistiken"
)
parser.add_argument(
    "-n",
    "--n-reveal",
    type=int,
    default=4,
    help="How many of the most attended people are revealed in anonymous stats.",
)
parser.add_argument(
    "-c",
    "--cut_response",
    default=None,
    help="allows to start calculating average response after" "specified date.",
)
args = parser.parse_args()

old_err_state = np.seterr(divide="raise")
ignored_states = np.seterr(**old_err_state)


def GetSections(df):
    sections = []
    df_T = df.T
    for i, col in enumerate(df_T):
        if i < 5:
            continue
        # if df_T[col].iloc[0] == "Teilnehmer":
        #     continue
        try:
            float_val = float(float(df_T[col].iloc[3]))
        except ValueError:
            continue
        if np.isnan(float_val):
            continue
        sections.append(df_T[col].iloc[0])
    return sections


def ReadKonzertmeisterStats():
    # read in attendance matrix from Konzertmeister app
    df = pd.read_csv(args.input_data, delimiter=args.delimiter)

    # Fill new df with information for every appointment
    # containing the following info: date, appointment_type, present, absent,
    # invited, response
    df_appointments = pd.DataFrame(
        columns=[
            "date",
            "appointment_type",
            "appointment_name",
            "present",
            "absent",
            "invited",
            "response",
        ]
    )

    # remove cancelled appointments
    indi = 0
    col_prev = ""
    drop_cols = []
    for col in df:
        if df[col].iloc[3] == "Anwesenheit" and df[col].iloc[4] == "0":
            drop_cols += [col, col_prev]
        elif (
            df[col].iloc[3] == "Anwesenheit"
            and int(df[col].iloc[4]) > 0
            and ("Probe" in col_prev or "Auftritt" in col_prev)
        ):
            date_string = df[col_prev].iloc[0].split(" ")[1]
            datetime_object = datetime.strptime(date_string, "%d.%m.%Y")
            count = Counter(df[col])
            count_prev = Counter(df[col_prev])
            # count = Counter(df[[col_prev, col]])
            # count = Counter(df.loc[:, [col_prev, col]])
            if "Probe" in col_prev:
                app_type = "Probe"
            elif "Auftritt" in col_prev:
                app_type = "Auftritt"
            else:
                print("wrong type in statistics")
                app_type = None
            app_dic = {
                "date": datetime_object,
                "appointment_type": app_type,
                "appointment_name": df[col_prev].iloc[2],
                "present": count["Anwesend"],
                "absent": count["Abwesend"],
                "invited": count["Anwesend"] + count["Abwesend"],
                "response": (
                    count_prev["Zusage"]
                    + count_prev["Absage"]
                    + count_prev["Vielleicht"]
                ),
            }
            df_appointments = df_appointments.append(app_dic, ignore_index=True)

        indi += 1
        col_prev = col
    df.drop(drop_cols, axis="columns", inplace=True)

    df_participants = pd.DataFrame(
        {
            "participant": (df.iloc[4:-1, [0]].values).flatten(),
            "pos_feedback": (df.iloc[4:-1, [1]].values).flatten(),
            "attendance": (df.iloc[4:-1, [2]].values).flatten(),
        }
    )

    sections = GetSections(df)

    sum_list = []
    for member, pos_feedback, attendance in zip(
        df_participants["participant"],
        df_participants["pos_feedback"],
        df_participants["attendance"],
    ):
        if member in sections:
            continue
        df_test = df.query(f"`Unnamed: 0`=='{member}'").T
        count = Counter(df_test.iloc[:, 0])
        sum_list.append(
            {
                "participant": member,
                "pos_feedbback": pos_feedback,
                "attendance": attendance,
                "present": count["Anwesend"],
                "absent": count["Abwesend"],
                "invited": count["Anwesend"] + count["Abwesend"],
                "response": (count["Zusage"] + count["Absage"] + count["Vielleicht"]),
            }
        )
    df_ = pd.DataFrame(sum_list)
    df = df.T
    df.rename(columns={3: "attendance_type"}, inplace=True)

    df.reset_index(drop=True, inplace=True)

    df_appointments["rel_attendance"] = (
        df_appointments["present"] / df_appointments["invited"]
    )

    return df_, df_appointments


def MatplotStyle():
    mtp.rcParams["font.size"] = 5
    mtp.rcParams["legend.frameon"] = False
    # mtp.rcParams['legend.fontsize'] = 14
    mtp.rcParams["lines.antialiased"] = False
    mtp.rcParams["lines.linewidth"] = 1
    # mtp.rcParams['xtick.direction'] = 'out'
    # mtp.rcParams['xtick.top'] = False
    # mtp.rcParams['xtick.minor.visible'] = False
    # mtp.rcParams['xtick.major.size'] = 10
    # mtp.rcParams['xtick.minor.size'] = 5
    # mtp.rcParams['ytick.direction'] = 'out'
    mtp.rcParams["ytick.right"] = True
    mtp.rcParams["ytick.minor.visible"] = True
    # mtp.rcParams['ytick.major.size'] = 10
    # mtp.rcParams['ytick.minor.size'] = 5
    # mtp.rcParams['mathtext.fontset'] = 'custom'


def AttendancePlot():
    _, df_appointments = ReadKonzertmeisterStats()
    cut_rehearsal = "appointment_type=='Probe'"
    cut_performance = "appointment_type=='Auftritt'"
    # average attendance
    MatplotStyle()
    avg_attendance = df_appointments.present.sum() / df_appointments.invited.sum()
    avg_attendance_rehearsal = (
        df_appointments.query(cut_rehearsal).present.sum()
        / df_appointments.query(cut_rehearsal).invited.sum()
    )
    avg_attendance_performance = np.divide(
        df_appointments.query(cut_performance).present.sum(),
        df_appointments.query(cut_performance).invited.sum(),
    )
    plt.plot(
        df_appointments.query(cut_rehearsal)["date"],
        (
            df_appointments.query(cut_rehearsal)["present"]
            / df_appointments.query(cut_rehearsal)["invited"]
            * 100
        ),
        ".",
        label="Probe",
    )
    plt.plot(
        df_appointments.query(cut_performance)["date"],
        (
            df_appointments.query(cut_performance)["present"]
            / df_appointments.query(cut_performance)["invited"]
            * 100
        ),
        ".",
        label="Auftritt",
        color="red",
    )
    plt.plot(
        df_appointments["date"],
        df_appointments["present"] / df_appointments["invited"] * 100,
        ":",
        alpha=0.2,
        color="gray",
    )
    plt.axhline(
        avg_attendance * 100,
        color="orange",
        label="mittlere Anwesenheit(%.1f%%)" % (avg_attendance * 100),
    )
    plt.axhline(
        avg_attendance_rehearsal * 100,
        color="#1f77b4",
        linestyle="--",
        alpha=0.6,
        label="mittlere Anwesenheit Proben (%.1f%%)" % (avg_attendance_rehearsal * 100),
    )
    plt.axhline(
        avg_attendance_performance * 100,
        color="red",
        linestyle="--",
        alpha=0.6,
        label="mittlere Anwesenheit Auftritte (%.1f%%)"
        % (avg_attendance_performance * 100),
    )
    n_performances = len(df_appointments.query(cut_performance))
    n_events = len(df_appointments)
    leg = plt.legend(
        title=f"Termine gesamt: {n_events}" f"(davon Auftritte {n_performances})"
    )
    leg._legend_box.align = "left"
    plt.xticks(rotation=45)
    plt.title(f"Anwesenheit {args.organisation} {args.output_name}")
    plt.grid(axis="x", alpha=0.5, linestyle="--")
    # plt.xlabel("Datum")
    plt.ylabel("Anwesenheit in %")
    plt.tight_layout()
    plt.savefig(
        f"{args.folder}/{args.output_name}_Uebersicht_Anwesenheit." f"{args.format}",
        transparent=False, dpi=300
    )
    plt.close()


def AttendancePerformance():
    _, df_appointments = ReadKonzertmeisterStats()
    cut_performance = "appointment_type=='Auftritt'"
    # average attendance
    MatplotStyle()
    mtp.rcParams["font.size"] = 5
    # mtp.rcParams['ytick.right'] = True
    avg_attendance_performance = np.divide(
        df_appointments.query(cut_performance).present.sum(),
        df_appointments.query(cut_performance).invited.sum(),
    )

    if args.cut_response is not None:
        df_appointments_response_cut = df_appointments.query(
            "date>='%s'" % args.cut_response
        )
        avg_response_performance = (
            df_appointments_response_cut.query(cut_performance).response.sum()
            / df_appointments_response_cut.query(cut_performance).invited.sum()
        )
    else:
        avg_response_performance = np.divide(
            df_appointments.query(cut_performance).response.sum(),
            df_appointments.query(cut_performance).invited.sum(),
        )
    plt.plot(
        df_appointments.query(cut_performance)["appointment_name"],
        (
            np.divide(
                df_appointments.query(cut_performance)["present"],
                df_appointments.query(cut_performance)["invited"],
            )
            * 100
        ),
        ".",
        color="red",
        label="Anwesenheit",
    )
    plt.plot(
        df_appointments.query(cut_performance)["appointment_name"],
        (
            np.divide(
                df_appointments.query(cut_performance)["present"],
                df_appointments.query(cut_performance)["invited"],
            )
            * 100
        ),
        ":",
        color="red",
        alpha=0.3,
    )

    plt.plot(
        df_appointments.query(cut_performance)["appointment_name"],
        (
            df_appointments.query(cut_performance)["response"]
            / df_appointments.query(cut_performance)["invited"]
            * 100
        ),
        ".",
        color="orange",
        label="Rückmeldung",
    )
    plt.plot(
        df_appointments.query(cut_performance)["appointment_name"],
        (
            df_appointments.query(cut_performance)["response"]
            / df_appointments.query(cut_performance)["invited"]
            * 100
        ),
        ":",
        color="orange",
        alpha=0.3,
    )
    plt.axhline(
        avg_attendance_performance * 100,
        color="red",
        linestyle="-",
        alpha=0.6,
        label="mittlere Anwesenheit Auftritte (%.1f%%)"
        % (avg_attendance_performance * 100),
    )
    if args.cut_response is not None:
        plt.axhline(
            avg_response_performance * 100,
            color="orange",
            linestyle="-",
            alpha=0.6,
            label="mittlere Rückmeldung Auftritte ab %s (%.1f%%)"
            % (args.cut_response, avg_response_performance * 100),
        )
    else:
        plt.axhline(
            avg_response_performance * 100,
            color="orange",
            linestyle="-",
            alpha=0.6,
            label="mittlere Rückmeldung Auftritte (%.1f%%)"
            % (avg_response_performance * 100),
        )
    n_performances = len(df_appointments.query(cut_performance))
    leg = plt.legend(title=f"Auftritte gesamt: {n_performances}")
    leg._legend_box.align = "left"
    plt.xticks(rotation=80)
    plt.title(f"Statistik Auftritte {args.organisation} {args.output_name}")
    plt.grid(axis="x", alpha=0.5, linestyle="--")
    # plt.xlabel("Datum")
    plt.ylabel("Anwesenheit/ Rückmeldung in %")
    plt.tight_layout()
    plt.savefig(
        f"{args.folder}/{args.output_name}_Statistik_Auftritte.{args.format}",
        transparent=False, dpi=300
    )
    plt.close()


def AttendanceRehearsal():
    _, df_appointments = ReadKonzertmeisterStats()
    cut_rehearsal = "appointment_type=='Probe'"
    # average attendance
    MatplotStyle()
    mtp.rcParams["font.size"] = 5
    avg_attendance_rehearsal = (
        df_appointments.query(cut_rehearsal).present.sum()
        / df_appointments.query(cut_rehearsal).invited.sum()
    )
    if args.cut_response is not None:
        df_appointments_response_cut = df_appointments.query(
            "date>='%s'" % args.cut_response
        )
        avg_response_rehearsal = (
            df_appointments_response_cut.query(cut_rehearsal).response.sum()
            / df_appointments_response_cut.query(cut_rehearsal).invited.sum()
        )
    else:
        avg_response_rehearsal = (
            df_appointments.query(cut_rehearsal).response.sum()
            / df_appointments.query(cut_rehearsal).invited.sum()
        )
    plt.plot(
        df_appointments.query(cut_rehearsal)["date"],
        (
            df_appointments.query(cut_rehearsal)["present"]
            / df_appointments.query(cut_rehearsal)["invited"]
            * 100
        ),
        ".",
        color="blue",
        label="Anwesenheit",
    )
    plt.plot(
        df_appointments.query(cut_rehearsal)["date"],
        (
            df_appointments.query(cut_rehearsal)["present"]
            / df_appointments.query(cut_rehearsal)["invited"]
            * 100
        ),
        ":",
        color="blue",
        alpha=0.3,
    )

    plt.plot(
        df_appointments.query(cut_rehearsal)["date"],
        (
            df_appointments.query(cut_rehearsal)["response"]
            / df_appointments.query(cut_rehearsal)["invited"]
            * 100
        ),
        ".",
        color="#1f77b4",
        label="Rückmeldung",
    )
    plt.plot(
        df_appointments.query(cut_rehearsal)["date"],
        (
            df_appointments.query(cut_rehearsal)["response"]
            / df_appointments.query(cut_rehearsal)["invited"]
            * 100
        ),
        ":",
        color="#1f77b4",
        alpha=0.3,
    )
    plt.axhline(
        avg_attendance_rehearsal * 100,
        color="blue",
        linestyle="-",
        alpha=0.6,
        label="mittlere Anwesenheit Proben (%.1f%%)" % (avg_attendance_rehearsal * 100),
    )
    if args.cut_response is not None:
        plt.axhline(
            avg_response_rehearsal * 100,
            color="#1f77b4",
            linestyle="-",
            alpha=0.6,
            label="mittlere Rückmeldung Proben ab %s (%.1f%%)"
            % (args.cut_response, avg_response_rehearsal * 100),
        )
    else:
        plt.axhline(
            avg_response_rehearsal * 100,
            color="#1f77b4",
            linestyle="-",
            alpha=0.6,
            label="mittlere Rückmeldung Proben (%.1f%%)"
            % (avg_response_rehearsal * 100),
        )
    leg = plt.legend(
        title=f"Proben gesamt: {len(df_appointments.query(cut_rehearsal))}"
    )
    leg._legend_box.align = "left"
    plt.xticks(rotation=45)
    plt.title(f"Statistik Proben {args.organisation} {args.output_name}")
    plt.grid(axis="x", alpha=0.5, linestyle="--")
    # plt.xlabel("Datum")
    plt.ylabel("Anwesenheit/ Rückmeldung in %")
    plt.tight_layout()
    plt.savefig(
        f"{args.folder}/{args.output_name}_Statistik_Proben.{args.format}",
        transparent=False, dpi=300
    )
    plt.close()


def AttendanceHist():
    df_participants, _ = ReadKonzertmeisterStats()
    rel_attendance = df_participants["present"] / df_participants["invited"] * 100
    min_ = np.min(rel_attendance)
    bins = np.linspace(0, 100, 21) - 2.5
    drop_low = np.where(bins >= min_)
    bins = bins[drop_low[0]]

    MatplotStyle()
    mtp.rcParams["font.size"] = 10
    plt.hist(rel_attendance, bins=bins, edgecolor="gray")

    plt.ylabel("Anzahl Musiker")
    plt.xlabel("Anwesenheit in %")
    plt.title(f"Anwesenheit {args.organisation} {args.output_name}")
    plt.tight_layout()
    plt.savefig(f"{args.folder}/{args.output_name}_relative_Anwesenheit.{args.format}")
    plt.close()

    plt.hist(df_participants["absent"], bins=20, edgecolor="gray")
    plt.ylabel("Anzahl Musiker")
    plt.xlabel("Fehltermine")
    plt.title(f"Fehltermine {args.organisation} {args.output_name}")
    plt.tight_layout()
    plt.savefig(f"{args.folder}/{args.output_name}_Fehltermine.{args.format}")

    df_participants["rel_attendance"] = (
        df_participants["present"] / df_participants["invited"] * 100
    )
    plt.close()


def AttendanceRankingTotal():
    df, _ = ReadKonzertmeisterStats()
    df.sort_values("absent", inplace=True)
    MatplotStyle()

    bar1 = plt.bar(df["participant"], df["absent"], edgecolor="gray")

    plt.rcParams["ytick.right"] = True

    # plt.bar(range(len(df_clicks)), df_clicks["clicks"], ".")
    for rect in bar1:
        height = rect.get_height()
        plt.text(
            rect.get_x() + rect.get_width() / 2.0,
            height,
            "%d" % int(height),
            ha="center",
            va="bottom",
            size=3,
        )

    mtp.rcParams["font.size"] = 4
    plt.xticks(rotation=80, fontsize=5)
    plt.yticks(fontsize=5)
    plt.ylabel("Fehltermine")
    plt.title(f"Statistik Fehltermine {args.organisation} {args.output_name}")
    plt.tight_layout()
    plt.savefig(f"{args.folder}/{args.output_name}_Ranking_Anwesenheit.{args.format}")
    plt.close()


def AttendanceRankingRelative():
    df, _ = ReadKonzertmeisterStats()
    df["rel_attendance"] = df["present"] / df["invited"] * 100
    df.sort_values("rel_attendance", inplace=True, ascending=False)
    MatplotStyle()
    anonymous_participants = []
    for i, memb in enumerate(df["participant"].values):
        if i < args.n_reveal:
            anonymous_participants.append(memb)
        else:
            anonymous_participants.append("")

    bar1 = plt.bar(
        df["participant"],
        df["rel_attendance"],
        edgecolor="gray",
        # color="#ffd79d"
        # color="#9D2EC5",
        color="orange",
    )

    plt.rcParams["ytick.right"] = True

    # plt.bar(range(len(df_clicks)), df_clicks["clicks"], ".")
    for absent_i, rect, invited_i in zip(
        df["absent"].values, bar1, df["invited"].values
    ):
        height = rect.get_height()
        if np.isnan(height):
            height = 0
        plt.text(
            rect.get_x() + rect.get_width() / 2.0,
            height,
            "%d%%" % int(height),
            ha="center",
            va="bottom",
            size=3,
        )

        height_long_text = 5
        if height < 20 and height >= 4:
            height_long_text = height + 3
        plt.text(
            rect.get_x() + rect.get_width() / 2.0,
            height_long_text,
            f"{absent_i} Fehltermine von {invited_i}",
            # f"{invited_i-absent_i}/{invited_i} anwesend",
            ha="center",
            va="bottom",
            size=3,
            rotation=90,
        )

    mtp.rcParams["font.size"] = 4
    if args.anonymous:
        plt.xticks(anonymous_participants, rotation=80, fontsize=5)
    else:
        plt.xticks(rotation=80, fontsize=5)
    plt.yticks(fontsize=5)
    plt.ylabel("Relative Anwesenheit in %")
    plt.title(f"Statistik Anwesenheit {args.organisation} {args.output_name}")
    plt.tight_layout()
    plt.savefig(
        f"{args.folder}/{args.output_name}_Ranking_Rel-Anwesenheit." f"{args.format}"
    )
    plt.close()


if __name__ == "__main__":
    os.makedirs(f"./{args.folder}", exist_ok=True)
    if args.anonymous is False:
        AttendancePlot()
        AttendancePerformance()
        AttendanceRehearsal()
        AttendanceHist()
        AttendanceRankingTotal()
    AttendanceRankingRelative()
